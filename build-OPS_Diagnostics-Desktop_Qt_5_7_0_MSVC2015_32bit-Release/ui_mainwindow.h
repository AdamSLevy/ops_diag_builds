/********************************************************************************
** Form generated from reading UI file 'mainwindow.ui'
**
** Created by: Qt User Interface Compiler version 5.7.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MAINWINDOW_H
#define UI_MAINWINDOW_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QFrame>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLCDNumber>
#include <QtWidgets/QLabel>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenu>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QProgressBar>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QTabWidget>
#include <QtWidgets/QTextBrowser>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>
#include "qcustomplot.h"

QT_BEGIN_NAMESPACE

class Ui_MainWindow
{
public:
    QAction *actionReconnect;
    QAction *actionChange_Log_Directory;
    QAction *actionSave_Log_File;
    QAction *actionNew_Log_File;
    QWidget *centralWidget;
    QVBoxLayout *verticalLayout_4;
    QFrame *dashFrame;
    QHBoxLayout *horizontalLayout_4;
    QPushButton *pushButton_4;
    QFrame *line;
    QPushButton *pushButton_2;
    QPushButton *pushButton;
    QPushButton *pushButton_3;
    QVBoxLayout *verticalLayout_9;
    QProgressBar *batteryChargeProgressBar;
    QHBoxLayout *horizontalLayout_3;
    QLCDNumber *batteryChargeLCD;
    QLabel *label;
    QVBoxLayout *verticalLayout_5;
    QHBoxLayout *horizontalLayout;
    QVBoxLayout *kwhLayout;
    QLCDNumber *kwhLCD;
    QLabel *kwhLabel;
    QVBoxLayout *packVoltageLayout;
    QLCDNumber *packVoltageLCD;
    QLabel *packVoltageLabel;
    QVBoxLayout *packCurrentLayout;
    QLCDNumber *packCurrentLCD;
    QLabel *packCurrentLabel;
    QHBoxLayout *horizontalLayout_2;
    QFrame *batteryIcon;
    QFrame *engineIcon;
    QFrame *contactorIcon;
    QFrame *keyIcon;
    QTabWidget *tabWidget;
    QWidget *packTab;
    QVBoxLayout *verticalLayout_3;
    QFrame *packCurrentFrame;
    QVBoxLayout *verticalLayout_6;
    QCustomPlot *packCurrentPlot;
    QFrame *packVoltageFrame;
    QVBoxLayout *verticalLayout_7;
    QCustomPlot *packVoltagePlot;
    QFrame *packTempFrame;
    QVBoxLayout *verticalLayout_8;
    QCustomPlot *packTempPlot;
    QWidget *cellTab;
    QVBoxLayout *verticalLayout_2;
    QCustomPlot *voltBarPlot;
    QCustomPlot *tempBarPlot;
    QWidget *logTab;
    QVBoxLayout *verticalLayout;
    QTextBrowser *statusLog;
    QMenuBar *menuBar;
    QMenu *menuFile;
    QStatusBar *statusBar;

    void setupUi(QMainWindow *MainWindow)
    {
        if (MainWindow->objectName().isEmpty())
            MainWindow->setObjectName(QStringLiteral("MainWindow"));
        MainWindow->resize(795, 1024);
        actionReconnect = new QAction(MainWindow);
        actionReconnect->setObjectName(QStringLiteral("actionReconnect"));
        actionChange_Log_Directory = new QAction(MainWindow);
        actionChange_Log_Directory->setObjectName(QStringLiteral("actionChange_Log_Directory"));
        actionSave_Log_File = new QAction(MainWindow);
        actionSave_Log_File->setObjectName(QStringLiteral("actionSave_Log_File"));
        actionNew_Log_File = new QAction(MainWindow);
        actionNew_Log_File->setObjectName(QStringLiteral("actionNew_Log_File"));
        actionNew_Log_File->setEnabled(false);
        centralWidget = new QWidget(MainWindow);
        centralWidget->setObjectName(QStringLiteral("centralWidget"));
        verticalLayout_4 = new QVBoxLayout(centralWidget);
        verticalLayout_4->setSpacing(6);
        verticalLayout_4->setContentsMargins(11, 11, 11, 11);
        verticalLayout_4->setObjectName(QStringLiteral("verticalLayout_4"));
        dashFrame = new QFrame(centralWidget);
        dashFrame->setObjectName(QStringLiteral("dashFrame"));
        dashFrame->setFrameShape(QFrame::StyledPanel);
        dashFrame->setFrameShadow(QFrame::Raised);
        horizontalLayout_4 = new QHBoxLayout(dashFrame);
        horizontalLayout_4->setSpacing(6);
        horizontalLayout_4->setContentsMargins(11, 11, 11, 11);
        horizontalLayout_4->setObjectName(QStringLiteral("horizontalLayout_4"));
        pushButton_4 = new QPushButton(dashFrame);
        pushButton_4->setObjectName(QStringLiteral("pushButton_4"));
        QSizePolicy sizePolicy(QSizePolicy::Minimum, QSizePolicy::Preferred);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(pushButton_4->sizePolicy().hasHeightForWidth());
        pushButton_4->setSizePolicy(sizePolicy);

        horizontalLayout_4->addWidget(pushButton_4);

        line = new QFrame(dashFrame);
        line->setObjectName(QStringLiteral("line"));
        line->setFrameShape(QFrame::VLine);
        line->setFrameShadow(QFrame::Sunken);

        horizontalLayout_4->addWidget(line);

        pushButton_2 = new QPushButton(dashFrame);
        pushButton_2->setObjectName(QStringLiteral("pushButton_2"));
        sizePolicy.setHeightForWidth(pushButton_2->sizePolicy().hasHeightForWidth());
        pushButton_2->setSizePolicy(sizePolicy);

        horizontalLayout_4->addWidget(pushButton_2);

        pushButton = new QPushButton(dashFrame);
        pushButton->setObjectName(QStringLiteral("pushButton"));
        sizePolicy.setHeightForWidth(pushButton->sizePolicy().hasHeightForWidth());
        pushButton->setSizePolicy(sizePolicy);

        horizontalLayout_4->addWidget(pushButton);

        pushButton_3 = new QPushButton(dashFrame);
        pushButton_3->setObjectName(QStringLiteral("pushButton_3"));
        sizePolicy.setHeightForWidth(pushButton_3->sizePolicy().hasHeightForWidth());
        pushButton_3->setSizePolicy(sizePolicy);

        horizontalLayout_4->addWidget(pushButton_3);

        verticalLayout_9 = new QVBoxLayout();
        verticalLayout_9->setSpacing(6);
        verticalLayout_9->setObjectName(QStringLiteral("verticalLayout_9"));
        batteryChargeProgressBar = new QProgressBar(dashFrame);
        batteryChargeProgressBar->setObjectName(QStringLiteral("batteryChargeProgressBar"));
        QSizePolicy sizePolicy1(QSizePolicy::MinimumExpanding, QSizePolicy::Expanding);
        sizePolicy1.setHorizontalStretch(0);
        sizePolicy1.setVerticalStretch(0);
        sizePolicy1.setHeightForWidth(batteryChargeProgressBar->sizePolicy().hasHeightForWidth());
        batteryChargeProgressBar->setSizePolicy(sizePolicy1);
        batteryChargeProgressBar->setMinimumSize(QSize(45, 0));
        batteryChargeProgressBar->setStyleSheet(QLatin1String("QProgressBar {\n"
"    border: 2px solid grey;\n"
"    border-radius: 3px;\n"
"    text-align: center;\n"
"}\n"
"\n"
"QProgressBar::chunk{\n"
"    height: 4px;\n"
"    margin: 0px;\n"
"    padding: 0px;\n"
"}\n"
"\n"
"\n"
"QProgressBar::chunk[charge=\"0\"]{\n"
"    background-color: qlineargradient(spread:reflect, x1:1, y1:0.516, x2:0.990291, y2:1, stop:0 rgba(255, 0, 0, 255), stop:1 rgba(204, 0, 0, 255));\n"
"}\n"
"\n"
"QProgressBar::chunk[charge=\"1\"]{\n"
"    background-color: qlineargradient(spread:reflect, x1:1, y1:1, x2:1, y2:0.511, stop:0 rgba(255, 204, 0, 255), stop:1 rgba(255, 255, 0, 255));\n"
"}\n"
"\n"
"QProgressBar::chunk[charge=\"2\"]{\n"
"    background-color: qlineargradient(spread:reflect, x1:1, y1:1, x2:1, y2:0.494, stop:0 rgba(0, 153, 0, 255), stop:1 rgba(0, 255, 0, 255));\n"
"}"));
        batteryChargeProgressBar->setValue(0);
        batteryChargeProgressBar->setAlignment(Qt::AlignCenter);
        batteryChargeProgressBar->setTextVisible(false);
        batteryChargeProgressBar->setOrientation(Qt::Vertical);
        batteryChargeProgressBar->setInvertedAppearance(false);
        batteryChargeProgressBar->setTextDirection(QProgressBar::TopToBottom);
        batteryChargeProgressBar->setProperty("charge", QVariant(2u));

        verticalLayout_9->addWidget(batteryChargeProgressBar);

        horizontalLayout_3 = new QHBoxLayout();
        horizontalLayout_3->setSpacing(6);
        horizontalLayout_3->setObjectName(QStringLiteral("horizontalLayout_3"));
        batteryChargeLCD = new QLCDNumber(dashFrame);
        batteryChargeLCD->setObjectName(QStringLiteral("batteryChargeLCD"));
        batteryChargeLCD->setMidLineWidth(0);
        batteryChargeLCD->setSmallDecimalPoint(false);
        batteryChargeLCD->setDigitCount(3);
        batteryChargeLCD->setSegmentStyle(QLCDNumber::Flat);
        batteryChargeLCD->setProperty("value", QVariant(0));
        batteryChargeLCD->setProperty("intValue", QVariant(0));

        horizontalLayout_3->addWidget(batteryChargeLCD);

        label = new QLabel(dashFrame);
        label->setObjectName(QStringLiteral("label"));

        horizontalLayout_3->addWidget(label);

        horizontalLayout_3->setStretch(0, 1);

        verticalLayout_9->addLayout(horizontalLayout_3);

        verticalLayout_9->setStretch(0, 1);

        horizontalLayout_4->addLayout(verticalLayout_9);

        verticalLayout_5 = new QVBoxLayout();
        verticalLayout_5->setSpacing(6);
        verticalLayout_5->setObjectName(QStringLiteral("verticalLayout_5"));
        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setSpacing(6);
        horizontalLayout->setObjectName(QStringLiteral("horizontalLayout"));
        kwhLayout = new QVBoxLayout();
        kwhLayout->setSpacing(6);
        kwhLayout->setObjectName(QStringLiteral("kwhLayout"));
        kwhLCD = new QLCDNumber(dashFrame);
        kwhLCD->setObjectName(QStringLiteral("kwhLCD"));
        kwhLCD->setDigitCount(4);
        kwhLCD->setSegmentStyle(QLCDNumber::Flat);

        kwhLayout->addWidget(kwhLCD);

        kwhLabel = new QLabel(dashFrame);
        kwhLabel->setObjectName(QStringLiteral("kwhLabel"));
        kwhLabel->setScaledContents(true);
        kwhLabel->setAlignment(Qt::AlignCenter);
        kwhLabel->setWordWrap(true);

        kwhLayout->addWidget(kwhLabel);

        kwhLayout->setStretch(0, 1);

        horizontalLayout->addLayout(kwhLayout);

        packVoltageLayout = new QVBoxLayout();
        packVoltageLayout->setSpacing(6);
        packVoltageLayout->setObjectName(QStringLiteral("packVoltageLayout"));
        packVoltageLCD = new QLCDNumber(dashFrame);
        packVoltageLCD->setObjectName(QStringLiteral("packVoltageLCD"));
        packVoltageLCD->setDigitCount(4);
        packVoltageLCD->setSegmentStyle(QLCDNumber::Flat);

        packVoltageLayout->addWidget(packVoltageLCD);

        packVoltageLabel = new QLabel(dashFrame);
        packVoltageLabel->setObjectName(QStringLiteral("packVoltageLabel"));
        packVoltageLabel->setScaledContents(true);
        packVoltageLabel->setAlignment(Qt::AlignCenter);
        packVoltageLabel->setWordWrap(true);

        packVoltageLayout->addWidget(packVoltageLabel);

        packVoltageLayout->setStretch(0, 1);

        horizontalLayout->addLayout(packVoltageLayout);

        packCurrentLayout = new QVBoxLayout();
        packCurrentLayout->setSpacing(6);
        packCurrentLayout->setObjectName(QStringLiteral("packCurrentLayout"));
        packCurrentLCD = new QLCDNumber(dashFrame);
        packCurrentLCD->setObjectName(QStringLiteral("packCurrentLCD"));
        packCurrentLCD->setDigitCount(4);
        packCurrentLCD->setSegmentStyle(QLCDNumber::Flat);

        packCurrentLayout->addWidget(packCurrentLCD);

        packCurrentLabel = new QLabel(dashFrame);
        packCurrentLabel->setObjectName(QStringLiteral("packCurrentLabel"));
        packCurrentLabel->setScaledContents(true);
        packCurrentLabel->setAlignment(Qt::AlignCenter);
        packCurrentLabel->setWordWrap(true);

        packCurrentLayout->addWidget(packCurrentLabel);

        packCurrentLayout->setStretch(0, 1);

        horizontalLayout->addLayout(packCurrentLayout);


        verticalLayout_5->addLayout(horizontalLayout);

        horizontalLayout_2 = new QHBoxLayout();
        horizontalLayout_2->setSpacing(6);
        horizontalLayout_2->setObjectName(QStringLiteral("horizontalLayout_2"));
        batteryIcon = new QFrame(dashFrame);
        batteryIcon->setObjectName(QStringLiteral("batteryIcon"));
        QSizePolicy sizePolicy2(QSizePolicy::Minimum, QSizePolicy::Minimum);
        sizePolicy2.setHorizontalStretch(0);
        sizePolicy2.setVerticalStretch(0);
        sizePolicy2.setHeightForWidth(batteryIcon->sizePolicy().hasHeightForWidth());
        batteryIcon->setSizePolicy(sizePolicy2);
        batteryIcon->setMinimumSize(QSize(40, 40));
        batteryIcon->setStyleSheet(QLatin1String("QFrame {\n"
"    padding: 2px;\n"
"    border-radius: 10px;\n"
"\n"
"}\n"
"\n"
"[isCharging=true]{\n"
"	background-color: qlineargradient(spread:reflect, x1:1, y1:0.470199, x2:0.990291, y2:1, stop:0 rgba(0, 204, 0, 255), stop:1 rgba(0, 255, 0, 255));\n"
"	image: url(:/dash_icons/dash_icons/battery_charging_icon.svg);\n"
"}\n"
"\n"
"[isCharging=false]{\n"
"	background-color: qlineargradient(spread:reflect, x1:1, y1:0.470199, x2:0.990291, y2:1, stop:0 rgba(153, 153, 153, 255), stop:1 rgba(204, 204, 204, 255));\n"
"	image: url(:/dash_icons/dash_icons/battery_icon.svg);\n"
"}"));
        batteryIcon->setFrameShape(QFrame::NoFrame);
        batteryIcon->setFrameShadow(QFrame::Plain);
        batteryIcon->setProperty("isCharging", QVariant(false));

        horizontalLayout_2->addWidget(batteryIcon);

        engineIcon = new QFrame(dashFrame);
        engineIcon->setObjectName(QStringLiteral("engineIcon"));
        sizePolicy2.setHeightForWidth(engineIcon->sizePolicy().hasHeightForWidth());
        engineIcon->setSizePolicy(sizePolicy2);
        engineIcon->setMinimumSize(QSize(40, 40));
        engineIcon->setStyleSheet(QLatin1String("QFrame {\n"
"    padding: 2px;\n"
"    border-radius: 10px;\n"
"	image: url(:/dash_icons/dash_icons/engine_icon.svg);\n"
"}\n"
"\n"
"[checkEngine=true]{\n"
"	background-color: qlineargradient(spread:reflect, x1:1, y1:0.470199, x2:0.990291, y2:1, stop:0 rgba(255, 102, 0, 255), stop:1 rgba(255, 153, 51, 255));\n"
"}\n"
"\n"
"[checkEngine=false]{\n"
"	background-color: qlineargradient(spread:reflect, x1:1, y1:0.470199, x2:0.990291, y2:1, stop:0 rgba(0, 204, 0, 255), stop:1 rgba(0, 255, 0, 255))\n"
"}"));
        engineIcon->setFrameShape(QFrame::NoFrame);
        engineIcon->setFrameShadow(QFrame::Plain);
        engineIcon->setProperty("checkEngine", QVariant(false));

        horizontalLayout_2->addWidget(engineIcon);

        contactorIcon = new QFrame(dashFrame);
        contactorIcon->setObjectName(QStringLiteral("contactorIcon"));
        sizePolicy2.setHeightForWidth(contactorIcon->sizePolicy().hasHeightForWidth());
        contactorIcon->setSizePolicy(sizePolicy2);
        contactorIcon->setMinimumSize(QSize(40, 40));
        contactorIcon->setStyleSheet(QLatin1String("QFrame {\n"
"    padding: 2px;\n"
"    border-radius: 10px;\n"
"	image: url(:/dash_icons/dash_icons/contactor_icon.svg);\n"
"}\n"
"\n"
"[contactorClosed=true]{\n"
"	background-color: qlineargradient(spread:reflect, x1:1, y1:0.470199, x2:0.990291, y2:1, stop:0 rgba(0, 204, 0, 255), stop:1 rgba(0, 255, 0, 255))\n"
"}\n"
"\n"
"[contactorClosed=false]{\n"
"	background-color: qlineargradient(spread:reflect, x1:1, y1:0.470199, x2:0.990291, y2:1, stop:0 rgba(153, 153, 153, 255), stop:1 rgba(204, 204, 204, 255))\n"
"}"));
        contactorIcon->setFrameShape(QFrame::NoFrame);
        contactorIcon->setFrameShadow(QFrame::Plain);
        contactorIcon->setProperty("contactorClosed", QVariant(false));

        horizontalLayout_2->addWidget(contactorIcon);

        keyIcon = new QFrame(dashFrame);
        keyIcon->setObjectName(QStringLiteral("keyIcon"));
        sizePolicy2.setHeightForWidth(keyIcon->sizePolicy().hasHeightForWidth());
        keyIcon->setSizePolicy(sizePolicy2);
        keyIcon->setMinimumSize(QSize(40, 40));
        keyIcon->setStyleSheet(QLatin1String("QFrame {\n"
"    padding: 2px;\n"
"    border-radius: 10px;\n"
"	image: url(:/dash_icons/dash_icons/key_icon.svg);\n"
"}\n"
"\n"
"[keyOn=true]{\n"
"	background-color: qlineargradient(spread:reflect, x1:1, y1:0.470199, x2:0.990291, y2:1, stop:0 rgba(0, 204, 0, 255), stop:1 rgba(0, 255, 0, 255))\n"
"}\n"
"\n"
"[keyOn=false]{\n"
"	background-color: qlineargradient(spread:reflect, x1:1, y1:0.470199, x2:0.990291, y2:1, stop:0 rgba(153, 153, 153, 255), stop:1 rgba(204, 204, 204, 255))\n"
"}"));
        keyIcon->setFrameShape(QFrame::NoFrame);
        keyIcon->setFrameShadow(QFrame::Plain);
        keyIcon->setProperty("keyOn", QVariant(false));

        horizontalLayout_2->addWidget(keyIcon);


        verticalLayout_5->addLayout(horizontalLayout_2);


        horizontalLayout_4->addLayout(verticalLayout_5);

        horizontalLayout_4->setStretch(0, 1);
        horizontalLayout_4->setStretch(1, 1);
        horizontalLayout_4->setStretch(2, 1);
        horizontalLayout_4->setStretch(3, 1);
        horizontalLayout_4->setStretch(4, 1);
        horizontalLayout_4->setStretch(6, 1);

        verticalLayout_4->addWidget(dashFrame);

        tabWidget = new QTabWidget(centralWidget);
        tabWidget->setObjectName(QStringLiteral("tabWidget"));
        QFont font;
        font.setPointSize(20);
        tabWidget->setFont(font);
        tabWidget->setStyleSheet(QLatin1String("QFrame {\n"
"\n"
"\n"
"}\n"
"\n"
"QFrame#packCurrentFrame {\n"
"	border: 1px solid black;\n"
"    border-radius: 8px;\n"
"    background-color: white;\n"
"	margin: 0px;\n"
"    padding: -8px;\n"
"}\n"
"\n"
"QFrame#packVoltageFrame {\n"
"	border: 1px solid black;\n"
"    border-radius: 8px;\n"
"    background-color: white;\n"
"	margin: 0px;\n"
"    padding: -8px;\n"
"}\n"
"\n"
"QFrame#packTempFrame {\n"
"	border: 1px solid black;\n"
"    border-radius: 8px;\n"
"    background-color: white;\n"
"	margin: 0px;\n"
"    padding: -8px;\n"
"}"));
        tabWidget->setTabPosition(QTabWidget::South);
        tabWidget->setElideMode(Qt::ElideMiddle);
        packTab = new QWidget();
        packTab->setObjectName(QStringLiteral("packTab"));
        verticalLayout_3 = new QVBoxLayout(packTab);
        verticalLayout_3->setSpacing(6);
        verticalLayout_3->setContentsMargins(11, 11, 11, 11);
        verticalLayout_3->setObjectName(QStringLiteral("verticalLayout_3"));
        packCurrentFrame = new QFrame(packTab);
        packCurrentFrame->setObjectName(QStringLiteral("packCurrentFrame"));
        packCurrentFrame->setStyleSheet(QStringLiteral(""));
        packCurrentFrame->setFrameShape(QFrame::StyledPanel);
        packCurrentFrame->setFrameShadow(QFrame::Raised);
        verticalLayout_6 = new QVBoxLayout(packCurrentFrame);
        verticalLayout_6->setSpacing(6);
        verticalLayout_6->setContentsMargins(11, 11, 11, 11);
        verticalLayout_6->setObjectName(QStringLiteral("verticalLayout_6"));
        packCurrentPlot = new QCustomPlot(packCurrentFrame);
        packCurrentPlot->setObjectName(QStringLiteral("packCurrentPlot"));

        verticalLayout_6->addWidget(packCurrentPlot);


        verticalLayout_3->addWidget(packCurrentFrame);

        packVoltageFrame = new QFrame(packTab);
        packVoltageFrame->setObjectName(QStringLiteral("packVoltageFrame"));
        packVoltageFrame->setStyleSheet(QStringLiteral(""));
        packVoltageFrame->setFrameShape(QFrame::StyledPanel);
        packVoltageFrame->setFrameShadow(QFrame::Raised);
        verticalLayout_7 = new QVBoxLayout(packVoltageFrame);
        verticalLayout_7->setSpacing(6);
        verticalLayout_7->setContentsMargins(11, 11, 11, 11);
        verticalLayout_7->setObjectName(QStringLiteral("verticalLayout_7"));
        packVoltagePlot = new QCustomPlot(packVoltageFrame);
        packVoltagePlot->setObjectName(QStringLiteral("packVoltagePlot"));
        packVoltagePlot->setStyleSheet(QStringLiteral(""));

        verticalLayout_7->addWidget(packVoltagePlot);


        verticalLayout_3->addWidget(packVoltageFrame);

        packTempFrame = new QFrame(packTab);
        packTempFrame->setObjectName(QStringLiteral("packTempFrame"));
        packTempFrame->setStyleSheet(QStringLiteral(""));
        packTempFrame->setFrameShape(QFrame::StyledPanel);
        packTempFrame->setFrameShadow(QFrame::Raised);
        verticalLayout_8 = new QVBoxLayout(packTempFrame);
        verticalLayout_8->setSpacing(6);
        verticalLayout_8->setContentsMargins(11, 11, 11, 11);
        verticalLayout_8->setObjectName(QStringLiteral("verticalLayout_8"));
        packTempPlot = new QCustomPlot(packTempFrame);
        packTempPlot->setObjectName(QStringLiteral("packTempPlot"));
        packTempPlot->setStyleSheet(QStringLiteral(""));

        verticalLayout_8->addWidget(packTempPlot);


        verticalLayout_3->addWidget(packTempFrame);

        tabWidget->addTab(packTab, QString());
        cellTab = new QWidget();
        cellTab->setObjectName(QStringLiteral("cellTab"));
        verticalLayout_2 = new QVBoxLayout(cellTab);
        verticalLayout_2->setSpacing(6);
        verticalLayout_2->setContentsMargins(11, 11, 11, 11);
        verticalLayout_2->setObjectName(QStringLiteral("verticalLayout_2"));
        voltBarPlot = new QCustomPlot(cellTab);
        voltBarPlot->setObjectName(QStringLiteral("voltBarPlot"));

        verticalLayout_2->addWidget(voltBarPlot);

        tempBarPlot = new QCustomPlot(cellTab);
        tempBarPlot->setObjectName(QStringLiteral("tempBarPlot"));

        verticalLayout_2->addWidget(tempBarPlot);

        tabWidget->addTab(cellTab, QString());
        logTab = new QWidget();
        logTab->setObjectName(QStringLiteral("logTab"));
        verticalLayout = new QVBoxLayout(logTab);
        verticalLayout->setSpacing(6);
        verticalLayout->setContentsMargins(11, 11, 11, 11);
        verticalLayout->setObjectName(QStringLiteral("verticalLayout"));
        statusLog = new QTextBrowser(logTab);
        statusLog->setObjectName(QStringLiteral("statusLog"));

        verticalLayout->addWidget(statusLog);

        tabWidget->addTab(logTab, QString());

        verticalLayout_4->addWidget(tabWidget);

        verticalLayout_4->setStretch(1, 1);
        MainWindow->setCentralWidget(centralWidget);
        menuBar = new QMenuBar(MainWindow);
        menuBar->setObjectName(QStringLiteral("menuBar"));
        menuBar->setGeometry(QRect(0, 0, 795, 22));
        menuFile = new QMenu(menuBar);
        menuFile->setObjectName(QStringLiteral("menuFile"));
        MainWindow->setMenuBar(menuBar);
        statusBar = new QStatusBar(MainWindow);
        statusBar->setObjectName(QStringLiteral("statusBar"));
        MainWindow->setStatusBar(statusBar);

        menuBar->addAction(menuFile->menuAction());
        menuFile->addAction(actionReconnect);
        menuFile->addAction(actionChange_Log_Directory);
        menuFile->addAction(actionNew_Log_File);

        retranslateUi(MainWindow);

        tabWidget->setCurrentIndex(0);


        QMetaObject::connectSlotsByName(MainWindow);
    } // setupUi

    void retranslateUi(QMainWindow *MainWindow)
    {
        MainWindow->setWindowTitle(QApplication::translate("MainWindow", "MainWindow", 0));
        actionReconnect->setText(QApplication::translate("MainWindow", "Reconnect", 0));
        actionChange_Log_Directory->setText(QApplication::translate("MainWindow", "Change Default Save Directory", 0));
        actionSave_Log_File->setText(QApplication::translate("MainWindow", "Save Log File", 0));
        actionNew_Log_File->setText(QApplication::translate("MainWindow", "Save and Create New Log File", 0));
        pushButton_4->setText(QApplication::translate("MainWindow", "Exit", 0));
        pushButton_2->setText(QApplication::translate("MainWindow", "Clear Faults", 0));
        pushButton->setText(QApplication::translate("MainWindow", "Go To Sleep", 0));
        pushButton_3->setText(QApplication::translate("MainWindow", "Close Contactors", 0));
        label->setText(QApplication::translate("MainWindow", "%", 0));
        kwhLabel->setText(QApplication::translate("MainWindow", "kWh Remaining", 0));
        packVoltageLabel->setText(QApplication::translate("MainWindow", "Pack Voltage", 0));
        packCurrentLabel->setText(QApplication::translate("MainWindow", "Pack Current", 0));
        tabWidget->setTabText(tabWidget->indexOf(packTab), QApplication::translate("MainWindow", "Pack", 0));
        tabWidget->setTabText(tabWidget->indexOf(cellTab), QApplication::translate("MainWindow", "Cells", 0));
        tabWidget->setTabText(tabWidget->indexOf(logTab), QApplication::translate("MainWindow", "Log", 0));
        menuFile->setTitle(QApplication::translate("MainWindow", "File", 0));
    } // retranslateUi

};

namespace Ui {
    class MainWindow: public Ui_MainWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MAINWINDOW_H
