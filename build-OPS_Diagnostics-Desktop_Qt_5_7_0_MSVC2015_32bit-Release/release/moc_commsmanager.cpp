/****************************************************************************
** Meta object code from reading C++ file 'commsmanager.hpp'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.7.0)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../../OPS_SW/commsmanager.hpp"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#include <QtCore/QVector>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'commsmanager.hpp' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.7.0. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
struct qt_meta_stringdata_CommsManager_t {
    QByteArrayData data[32];
    char stringdata0[363];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_CommsManager_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_CommsManager_t qt_meta_stringdata_CommsManager = {
    {
QT_MOC_LITERAL(0, 0, 12), // "CommsManager"
QT_MOC_LITERAL(1, 13, 15), // "faultRegChanged"
QT_MOC_LITERAL(2, 29, 0), // ""
QT_MOC_LITERAL(3, 30, 7), // "uint8_t"
QT_MOC_LITERAL(4, 38, 8), // "fault_id"
QT_MOC_LITERAL(5, 47, 8), // "uint16_t"
QT_MOC_LITERAL(6, 56, 9), // "fault_reg"
QT_MOC_LITERAL(7, 66, 15), // "instpRegChanged"
QT_MOC_LITERAL(8, 82, 12), // "instp_reg_id"
QT_MOC_LITERAL(9, 95, 9), // "instp_reg"
QT_MOC_LITERAL(10, 105, 14), // "cellRegChanged"
QT_MOC_LITERAL(11, 120, 11), // "cell_reg_id"
QT_MOC_LITERAL(12, 132, 8), // "cell_reg"
QT_MOC_LITERAL(13, 141, 15), // "numTempsChanged"
QT_MOC_LITERAL(14, 157, 9), // "num_temps"
QT_MOC_LITERAL(15, 167, 15), // "numVoltsChanged"
QT_MOC_LITERAL(16, 183, 9), // "num_volts"
QT_MOC_LITERAL(17, 193, 7), // "allData"
QT_MOC_LITERAL(18, 201, 7), // "time_in"
QT_MOC_LITERAL(19, 209, 15), // "QVector<double>"
QT_MOC_LITERAL(20, 225, 5), // "lifet"
QT_MOC_LITERAL(21, 231, 5), // "instp"
QT_MOC_LITERAL(22, 237, 5), // "volts"
QT_MOC_LITERAL(23, 243, 5), // "temps"
QT_MOC_LITERAL(24, 249, 9), // "connected"
QT_MOC_LITERAL(25, 259, 12), // "disconnected"
QT_MOC_LITERAL(26, 272, 12), // "portNotFound"
QT_MOC_LITERAL(27, 285, 10), // "failedOpen"
QT_MOC_LITERAL(28, 296, 28), // "QSerialPort::SerialPortError"
QT_MOC_LITERAL(29, 325, 15), // "handleReadyRead"
QT_MOC_LITERAL(30, 341, 11), // "handleError"
QT_MOC_LITERAL(31, 353, 9) // "reconnect"

    },
    "CommsManager\0faultRegChanged\0\0uint8_t\0"
    "fault_id\0uint16_t\0fault_reg\0instpRegChanged\0"
    "instp_reg_id\0instp_reg\0cellRegChanged\0"
    "cell_reg_id\0cell_reg\0numTempsChanged\0"
    "num_temps\0numVoltsChanged\0num_volts\0"
    "allData\0time_in\0QVector<double>\0lifet\0"
    "instp\0volts\0temps\0connected\0disconnected\0"
    "portNotFound\0failedOpen\0"
    "QSerialPort::SerialPortError\0"
    "handleReadyRead\0handleError\0reconnect"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_CommsManager[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
      13,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
      10,       // signalCount

 // signals: name, argc, parameters, tag, flags
       1,    2,   79,    2, 0x06 /* Public */,
       7,    2,   84,    2, 0x06 /* Public */,
      10,    2,   89,    2, 0x06 /* Public */,
      13,    1,   94,    2, 0x06 /* Public */,
      15,    1,   97,    2, 0x06 /* Public */,
      17,    5,  100,    2, 0x06 /* Public */,
      24,    0,  111,    2, 0x06 /* Public */,
      25,    0,  112,    2, 0x06 /* Public */,
      26,    0,  113,    2, 0x06 /* Public */,
      27,    2,  114,    2, 0x06 /* Public */,

 // slots: name, argc, parameters, tag, flags
      29,    0,  119,    2, 0x08 /* Private */,
      30,    1,  120,    2, 0x08 /* Private */,
      31,    0,  123,    2, 0x0a /* Public */,

 // signals: parameters
    QMetaType::Void, 0x80000000 | 3, 0x80000000 | 5,    4,    6,
    QMetaType::Void, 0x80000000 | 3, 0x80000000 | 5,    8,    9,
    QMetaType::Void, 0x80000000 | 3, 0x80000000 | 5,   11,   12,
    QMetaType::Void, 0x80000000 | 5,   14,
    QMetaType::Void, 0x80000000 | 5,   16,
    QMetaType::Void, QMetaType::QDateTime, 0x80000000 | 19, 0x80000000 | 19, 0x80000000 | 19, 0x80000000 | 19,   18,   20,   21,   22,   23,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, 0x80000000 | 28, QMetaType::QString,    2,    2,

 // slots: parameters
    QMetaType::Void,
    QMetaType::Void, 0x80000000 | 28,    2,
    QMetaType::Void,

       0        // eod
};

void CommsManager::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        CommsManager *_t = static_cast<CommsManager *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->faultRegChanged((*reinterpret_cast< uint8_t(*)>(_a[1])),(*reinterpret_cast< uint16_t(*)>(_a[2]))); break;
        case 1: _t->instpRegChanged((*reinterpret_cast< uint8_t(*)>(_a[1])),(*reinterpret_cast< uint16_t(*)>(_a[2]))); break;
        case 2: _t->cellRegChanged((*reinterpret_cast< uint8_t(*)>(_a[1])),(*reinterpret_cast< uint16_t(*)>(_a[2]))); break;
        case 3: _t->numTempsChanged((*reinterpret_cast< uint16_t(*)>(_a[1]))); break;
        case 4: _t->numVoltsChanged((*reinterpret_cast< uint16_t(*)>(_a[1]))); break;
        case 5: _t->allData((*reinterpret_cast< QDateTime(*)>(_a[1])),(*reinterpret_cast< QVector<double>(*)>(_a[2])),(*reinterpret_cast< QVector<double>(*)>(_a[3])),(*reinterpret_cast< QVector<double>(*)>(_a[4])),(*reinterpret_cast< QVector<double>(*)>(_a[5]))); break;
        case 6: _t->connected(); break;
        case 7: _t->disconnected(); break;
        case 8: _t->portNotFound(); break;
        case 9: _t->failedOpen((*reinterpret_cast< QSerialPort::SerialPortError(*)>(_a[1])),(*reinterpret_cast< QString(*)>(_a[2]))); break;
        case 10: _t->handleReadyRead(); break;
        case 11: _t->handleError((*reinterpret_cast< QSerialPort::SerialPortError(*)>(_a[1]))); break;
        case 12: _t->reconnect(); break;
        default: ;
        }
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        switch (_id) {
        default: *reinterpret_cast<int*>(_a[0]) = -1; break;
        case 5:
            switch (*reinterpret_cast<int*>(_a[1])) {
            default: *reinterpret_cast<int*>(_a[0]) = -1; break;
            case 4:
            case 3:
            case 2:
            case 1:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< QVector<double> >(); break;
            }
            break;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        void **func = reinterpret_cast<void **>(_a[1]);
        {
            typedef void (CommsManager::*_t)(uint8_t , uint16_t );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&CommsManager::faultRegChanged)) {
                *result = 0;
                return;
            }
        }
        {
            typedef void (CommsManager::*_t)(uint8_t , uint16_t );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&CommsManager::instpRegChanged)) {
                *result = 1;
                return;
            }
        }
        {
            typedef void (CommsManager::*_t)(uint8_t , uint16_t );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&CommsManager::cellRegChanged)) {
                *result = 2;
                return;
            }
        }
        {
            typedef void (CommsManager::*_t)(uint16_t );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&CommsManager::numTempsChanged)) {
                *result = 3;
                return;
            }
        }
        {
            typedef void (CommsManager::*_t)(uint16_t );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&CommsManager::numVoltsChanged)) {
                *result = 4;
                return;
            }
        }
        {
            typedef void (CommsManager::*_t)(QDateTime , QVector<double> , QVector<double> , QVector<double> , QVector<double> );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&CommsManager::allData)) {
                *result = 5;
                return;
            }
        }
        {
            typedef void (CommsManager::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&CommsManager::connected)) {
                *result = 6;
                return;
            }
        }
        {
            typedef void (CommsManager::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&CommsManager::disconnected)) {
                *result = 7;
                return;
            }
        }
        {
            typedef void (CommsManager::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&CommsManager::portNotFound)) {
                *result = 8;
                return;
            }
        }
        {
            typedef void (CommsManager::*_t)(QSerialPort::SerialPortError , QString );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&CommsManager::failedOpen)) {
                *result = 9;
                return;
            }
        }
    }
}

const QMetaObject CommsManager::staticMetaObject = {
    { &QObject::staticMetaObject, qt_meta_stringdata_CommsManager.data,
      qt_meta_data_CommsManager,  qt_static_metacall, Q_NULLPTR, Q_NULLPTR}
};


const QMetaObject *CommsManager::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *CommsManager::qt_metacast(const char *_clname)
{
    if (!_clname) return Q_NULLPTR;
    if (!strcmp(_clname, qt_meta_stringdata_CommsManager.stringdata0))
        return static_cast<void*>(const_cast< CommsManager*>(this));
    return QObject::qt_metacast(_clname);
}

int CommsManager::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QObject::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 13)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 13;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 13)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 13;
    }
    return _id;
}

// SIGNAL 0
void CommsManager::faultRegChanged(uint8_t _t1, uint16_t _t2)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)), const_cast<void*>(reinterpret_cast<const void*>(&_t2)) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}

// SIGNAL 1
void CommsManager::instpRegChanged(uint8_t _t1, uint16_t _t2)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)), const_cast<void*>(reinterpret_cast<const void*>(&_t2)) };
    QMetaObject::activate(this, &staticMetaObject, 1, _a);
}

// SIGNAL 2
void CommsManager::cellRegChanged(uint8_t _t1, uint16_t _t2)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)), const_cast<void*>(reinterpret_cast<const void*>(&_t2)) };
    QMetaObject::activate(this, &staticMetaObject, 2, _a);
}

// SIGNAL 3
void CommsManager::numTempsChanged(uint16_t _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 3, _a);
}

// SIGNAL 4
void CommsManager::numVoltsChanged(uint16_t _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 4, _a);
}

// SIGNAL 5
void CommsManager::allData(QDateTime _t1, QVector<double> _t2, QVector<double> _t3, QVector<double> _t4, QVector<double> _t5)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)), const_cast<void*>(reinterpret_cast<const void*>(&_t2)), const_cast<void*>(reinterpret_cast<const void*>(&_t3)), const_cast<void*>(reinterpret_cast<const void*>(&_t4)), const_cast<void*>(reinterpret_cast<const void*>(&_t5)) };
    QMetaObject::activate(this, &staticMetaObject, 5, _a);
}

// SIGNAL 6
void CommsManager::connected()
{
    QMetaObject::activate(this, &staticMetaObject, 6, Q_NULLPTR);
}

// SIGNAL 7
void CommsManager::disconnected()
{
    QMetaObject::activate(this, &staticMetaObject, 7, Q_NULLPTR);
}

// SIGNAL 8
void CommsManager::portNotFound()
{
    QMetaObject::activate(this, &staticMetaObject, 8, Q_NULLPTR);
}

// SIGNAL 9
void CommsManager::failedOpen(QSerialPort::SerialPortError _t1, QString _t2)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)), const_cast<void*>(reinterpret_cast<const void*>(&_t2)) };
    QMetaObject::activate(this, &staticMetaObject, 9, _a);
}
QT_END_MOC_NAMESPACE
