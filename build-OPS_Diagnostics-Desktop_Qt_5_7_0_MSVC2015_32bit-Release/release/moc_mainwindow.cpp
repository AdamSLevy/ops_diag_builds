/****************************************************************************
** Meta object code from reading C++ file 'mainwindow.hpp'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.7.0)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../../OPS_SW/mainwindow.hpp"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#include <QtCore/QVector>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'mainwindow.hpp' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.7.0. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
struct qt_meta_stringdata_MainWindow_t {
    QByteArrayData data[37];
    char stringdata0[475];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_MainWindow_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_MainWindow_t qt_meta_stringdata_MainWindow = {
    {
QT_MOC_LITERAL(0, 0, 10), // "MainWindow"
QT_MOC_LITERAL(1, 11, 10), // "newLogFile"
QT_MOC_LITERAL(2, 22, 0), // ""
QT_MOC_LITERAL(3, 23, 13), // "logDirChanged"
QT_MOC_LITERAL(4, 37, 4), // "QDir"
QT_MOC_LITERAL(5, 42, 11), // "saveLogFile"
QT_MOC_LITERAL(6, 54, 12), // "saveFilePath"
QT_MOC_LITERAL(7, 67, 13), // "deleteLogFile"
QT_MOC_LITERAL(8, 81, 22), // "handleNewLogFileOpened"
QT_MOC_LITERAL(9, 104, 8), // "saveName"
QT_MOC_LITERAL(10, 113, 22), // "handleCreateNewLogFile"
QT_MOC_LITERAL(11, 136, 18), // "handlePortNotFound"
QT_MOC_LITERAL(12, 155, 15), // "handleConnected"
QT_MOC_LITERAL(13, 171, 18), // "handleDisconnected"
QT_MOC_LITERAL(14, 190, 16), // "handleFailedOpen"
QT_MOC_LITERAL(15, 207, 28), // "QSerialPort::SerialPortError"
QT_MOC_LITERAL(16, 236, 11), // "errorString"
QT_MOC_LITERAL(17, 248, 21), // "handleFaultRegChanged"
QT_MOC_LITERAL(18, 270, 7), // "uint8_t"
QT_MOC_LITERAL(19, 278, 8), // "fault_id"
QT_MOC_LITERAL(20, 287, 8), // "uint16_t"
QT_MOC_LITERAL(21, 296, 9), // "fault_reg"
QT_MOC_LITERAL(22, 306, 21), // "handleInstpRegChanged"
QT_MOC_LITERAL(23, 328, 12), // "instp_reg_id"
QT_MOC_LITERAL(24, 341, 9), // "instp_reg"
QT_MOC_LITERAL(25, 351, 20), // "handleCellRegChanged"
QT_MOC_LITERAL(26, 372, 11), // "cell_reg_id"
QT_MOC_LITERAL(27, 384, 8), // "cell_reg"
QT_MOC_LITERAL(28, 393, 13), // "handleAllData"
QT_MOC_LITERAL(29, 407, 7), // "time_in"
QT_MOC_LITERAL(30, 415, 15), // "QVector<double>"
QT_MOC_LITERAL(31, 431, 5), // "lifet"
QT_MOC_LITERAL(32, 437, 5), // "instp"
QT_MOC_LITERAL(33, 443, 5), // "volts"
QT_MOC_LITERAL(34, 449, 5), // "temps"
QT_MOC_LITERAL(35, 455, 9), // "replotAll"
QT_MOC_LITERAL(36, 465, 9) // "setLogDir"

    },
    "MainWindow\0newLogFile\0\0logDirChanged\0"
    "QDir\0saveLogFile\0saveFilePath\0"
    "deleteLogFile\0handleNewLogFileOpened\0"
    "saveName\0handleCreateNewLogFile\0"
    "handlePortNotFound\0handleConnected\0"
    "handleDisconnected\0handleFailedOpen\0"
    "QSerialPort::SerialPortError\0errorString\0"
    "handleFaultRegChanged\0uint8_t\0fault_id\0"
    "uint16_t\0fault_reg\0handleInstpRegChanged\0"
    "instp_reg_id\0instp_reg\0handleCellRegChanged\0"
    "cell_reg_id\0cell_reg\0handleAllData\0"
    "time_in\0QVector<double>\0lifet\0instp\0"
    "volts\0temps\0replotAll\0setLogDir"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_MainWindow[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
      17,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       5,       // signalCount

 // signals: name, argc, parameters, tag, flags
       1,    0,   99,    2, 0x06 /* Public */,
       3,    1,  100,    2, 0x06 /* Public */,
       5,    1,  103,    2, 0x06 /* Public */,
       5,    0,  106,    2, 0x26 /* Public | MethodCloned */,
       7,    0,  107,    2, 0x06 /* Public */,

 // slots: name, argc, parameters, tag, flags
       8,    1,  108,    2, 0x0a /* Public */,
      10,    0,  111,    2, 0x0a /* Public */,
      11,    0,  112,    2, 0x0a /* Public */,
      12,    0,  113,    2, 0x0a /* Public */,
      13,    0,  114,    2, 0x0a /* Public */,
      14,    2,  115,    2, 0x0a /* Public */,
      17,    2,  120,    2, 0x0a /* Public */,
      22,    2,  125,    2, 0x0a /* Public */,
      25,    2,  130,    2, 0x0a /* Public */,
      28,    5,  135,    2, 0x0a /* Public */,
      35,    0,  146,    2, 0x0a /* Public */,
      36,    0,  147,    2, 0x0a /* Public */,

 // signals: parameters
    QMetaType::Void,
    QMetaType::Void, 0x80000000 | 4,    2,
    QMetaType::Void, QMetaType::QString,    6,
    QMetaType::Void,
    QMetaType::Void,

 // slots: parameters
    QMetaType::Void, QMetaType::QString,    9,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, 0x80000000 | 15, QMetaType::QString,    2,   16,
    QMetaType::Void, 0x80000000 | 18, 0x80000000 | 20,   19,   21,
    QMetaType::Void, 0x80000000 | 18, 0x80000000 | 20,   23,   24,
    QMetaType::Void, 0x80000000 | 18, 0x80000000 | 20,   26,   27,
    QMetaType::Void, QMetaType::QDateTime, 0x80000000 | 30, 0x80000000 | 30, 0x80000000 | 30, 0x80000000 | 30,   29,   31,   32,   33,   34,
    QMetaType::Void,
    QMetaType::Void,

       0        // eod
};

void MainWindow::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        MainWindow *_t = static_cast<MainWindow *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->newLogFile(); break;
        case 1: _t->logDirChanged((*reinterpret_cast< QDir(*)>(_a[1]))); break;
        case 2: _t->saveLogFile((*reinterpret_cast< QString(*)>(_a[1]))); break;
        case 3: _t->saveLogFile(); break;
        case 4: _t->deleteLogFile(); break;
        case 5: _t->handleNewLogFileOpened((*reinterpret_cast< QString(*)>(_a[1]))); break;
        case 6: _t->handleCreateNewLogFile(); break;
        case 7: _t->handlePortNotFound(); break;
        case 8: _t->handleConnected(); break;
        case 9: _t->handleDisconnected(); break;
        case 10: _t->handleFailedOpen((*reinterpret_cast< QSerialPort::SerialPortError(*)>(_a[1])),(*reinterpret_cast< QString(*)>(_a[2]))); break;
        case 11: _t->handleFaultRegChanged((*reinterpret_cast< uint8_t(*)>(_a[1])),(*reinterpret_cast< uint16_t(*)>(_a[2]))); break;
        case 12: _t->handleInstpRegChanged((*reinterpret_cast< uint8_t(*)>(_a[1])),(*reinterpret_cast< uint16_t(*)>(_a[2]))); break;
        case 13: _t->handleCellRegChanged((*reinterpret_cast< uint8_t(*)>(_a[1])),(*reinterpret_cast< uint16_t(*)>(_a[2]))); break;
        case 14: _t->handleAllData((*reinterpret_cast< QDateTime(*)>(_a[1])),(*reinterpret_cast< QVector<double>(*)>(_a[2])),(*reinterpret_cast< QVector<double>(*)>(_a[3])),(*reinterpret_cast< QVector<double>(*)>(_a[4])),(*reinterpret_cast< QVector<double>(*)>(_a[5]))); break;
        case 15: _t->replotAll(); break;
        case 16: _t->setLogDir(); break;
        default: ;
        }
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        switch (_id) {
        default: *reinterpret_cast<int*>(_a[0]) = -1; break;
        case 14:
            switch (*reinterpret_cast<int*>(_a[1])) {
            default: *reinterpret_cast<int*>(_a[0]) = -1; break;
            case 4:
            case 3:
            case 2:
            case 1:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< QVector<double> >(); break;
            }
            break;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        void **func = reinterpret_cast<void **>(_a[1]);
        {
            typedef void (MainWindow::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&MainWindow::newLogFile)) {
                *result = 0;
                return;
            }
        }
        {
            typedef void (MainWindow::*_t)(QDir );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&MainWindow::logDirChanged)) {
                *result = 1;
                return;
            }
        }
        {
            typedef void (MainWindow::*_t)(QString );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&MainWindow::saveLogFile)) {
                *result = 2;
                return;
            }
        }
        {
            typedef void (MainWindow::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&MainWindow::deleteLogFile)) {
                *result = 4;
                return;
            }
        }
    }
}

const QMetaObject MainWindow::staticMetaObject = {
    { &QMainWindow::staticMetaObject, qt_meta_stringdata_MainWindow.data,
      qt_meta_data_MainWindow,  qt_static_metacall, Q_NULLPTR, Q_NULLPTR}
};


const QMetaObject *MainWindow::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *MainWindow::qt_metacast(const char *_clname)
{
    if (!_clname) return Q_NULLPTR;
    if (!strcmp(_clname, qt_meta_stringdata_MainWindow.stringdata0))
        return static_cast<void*>(const_cast< MainWindow*>(this));
    return QMainWindow::qt_metacast(_clname);
}

int MainWindow::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QMainWindow::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 17)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 17;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 17)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 17;
    }
    return _id;
}

// SIGNAL 0
void MainWindow::newLogFile()
{
    QMetaObject::activate(this, &staticMetaObject, 0, Q_NULLPTR);
}

// SIGNAL 1
void MainWindow::logDirChanged(QDir _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 1, _a);
}

// SIGNAL 2
void MainWindow::saveLogFile(QString _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 2, _a);
}

// SIGNAL 4
void MainWindow::deleteLogFile()
{
    QMetaObject::activate(this, &staticMetaObject, 4, Q_NULLPTR);
}
QT_END_MOC_NAMESPACE
