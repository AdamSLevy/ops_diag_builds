/****************************************************************************
** Meta object code from reading C++ file 'datalogger.hpp'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.7.0)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../../OPS_SW/datalogger.hpp"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#include <QtCore/QVector>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'datalogger.hpp' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.7.0. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
struct qt_meta_stringdata_DataLogger_t {
    QByteArrayData data[33];
    char stringdata0[386];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_DataLogger_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_DataLogger_t qt_meta_stringdata_DataLogger = {
    {
QT_MOC_LITERAL(0, 0, 10), // "DataLogger"
QT_MOC_LITERAL(1, 11, 16), // "newLogFileOpened"
QT_MOC_LITERAL(2, 28, 0), // ""
QT_MOC_LITERAL(3, 29, 8), // "saveName"
QT_MOC_LITERAL(4, 38, 12), // "failedToOpen"
QT_MOC_LITERAL(5, 51, 11), // "errorString"
QT_MOC_LITERAL(6, 63, 21), // "handleFaultRegChanged"
QT_MOC_LITERAL(7, 85, 7), // "uint8_t"
QT_MOC_LITERAL(8, 93, 8), // "fault_id"
QT_MOC_LITERAL(9, 102, 8), // "uint16_t"
QT_MOC_LITERAL(10, 111, 9), // "fault_reg"
QT_MOC_LITERAL(11, 121, 21), // "handleInstpRegChanged"
QT_MOC_LITERAL(12, 143, 12), // "instp_reg_id"
QT_MOC_LITERAL(13, 156, 9), // "instp_reg"
QT_MOC_LITERAL(14, 166, 20), // "handleCellRegChanged"
QT_MOC_LITERAL(15, 187, 11), // "cell_reg_id"
QT_MOC_LITERAL(16, 199, 8), // "cell_reg"
QT_MOC_LITERAL(17, 208, 13), // "handleAllData"
QT_MOC_LITERAL(18, 222, 7), // "time_in"
QT_MOC_LITERAL(19, 230, 15), // "QVector<double>"
QT_MOC_LITERAL(20, 246, 5), // "lifet"
QT_MOC_LITERAL(21, 252, 5), // "instp"
QT_MOC_LITERAL(22, 258, 5), // "volts"
QT_MOC_LITERAL(23, 264, 5), // "temps"
QT_MOC_LITERAL(24, 270, 19), // "handleLogDirChanged"
QT_MOC_LITERAL(25, 290, 4), // "QDir"
QT_MOC_LITERAL(26, 295, 7), // "log_dir"
QT_MOC_LITERAL(27, 303, 17), // "handleSaveLogFile"
QT_MOC_LITERAL(28, 321, 12), // "saveFilePath"
QT_MOC_LITERAL(29, 334, 11), // "openXmlFile"
QT_MOC_LITERAL(30, 346, 12), // "closeXmlFile"
QT_MOC_LITERAL(31, 359, 13), // "deleteLogFile"
QT_MOC_LITERAL(32, 373, 12) // "writeXmlData"

    },
    "DataLogger\0newLogFileOpened\0\0saveName\0"
    "failedToOpen\0errorString\0handleFaultRegChanged\0"
    "uint8_t\0fault_id\0uint16_t\0fault_reg\0"
    "handleInstpRegChanged\0instp_reg_id\0"
    "instp_reg\0handleCellRegChanged\0"
    "cell_reg_id\0cell_reg\0handleAllData\0"
    "time_in\0QVector<double>\0lifet\0instp\0"
    "volts\0temps\0handleLogDirChanged\0QDir\0"
    "log_dir\0handleSaveLogFile\0saveFilePath\0"
    "openXmlFile\0closeXmlFile\0deleteLogFile\0"
    "writeXmlData"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_DataLogger[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
      13,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       2,       // signalCount

 // signals: name, argc, parameters, tag, flags
       1,    1,   79,    2, 0x06 /* Public */,
       4,    1,   82,    2, 0x06 /* Public */,

 // slots: name, argc, parameters, tag, flags
       6,    2,   85,    2, 0x0a /* Public */,
      11,    2,   90,    2, 0x0a /* Public */,
      14,    2,   95,    2, 0x0a /* Public */,
      17,    5,  100,    2, 0x0a /* Public */,
      24,    1,  111,    2, 0x0a /* Public */,
      27,    1,  114,    2, 0x0a /* Public */,
      29,    0,  117,    2, 0x0a /* Public */,
      30,    1,  118,    2, 0x0a /* Public */,
      30,    0,  121,    2, 0x2a /* Public | MethodCloned */,
      31,    0,  122,    2, 0x0a /* Public */,
      32,    0,  123,    2, 0x08 /* Private */,

 // signals: parameters
    QMetaType::Void, QMetaType::QString,    3,
    QMetaType::Void, QMetaType::QString,    5,

 // slots: parameters
    QMetaType::Void, 0x80000000 | 7, 0x80000000 | 9,    8,   10,
    QMetaType::Void, 0x80000000 | 7, 0x80000000 | 9,   12,   13,
    QMetaType::Void, 0x80000000 | 7, 0x80000000 | 9,   15,   16,
    QMetaType::Void, QMetaType::QDateTime, 0x80000000 | 19, 0x80000000 | 19, 0x80000000 | 19, 0x80000000 | 19,   18,   20,   21,   22,   23,
    QMetaType::Void, 0x80000000 | 25,   26,
    QMetaType::Void, QMetaType::QString,   28,
    QMetaType::Bool,
    QMetaType::Void, QMetaType::QString,   28,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,

       0        // eod
};

void DataLogger::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        DataLogger *_t = static_cast<DataLogger *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->newLogFileOpened((*reinterpret_cast< QString(*)>(_a[1]))); break;
        case 1: _t->failedToOpen((*reinterpret_cast< QString(*)>(_a[1]))); break;
        case 2: _t->handleFaultRegChanged((*reinterpret_cast< uint8_t(*)>(_a[1])),(*reinterpret_cast< uint16_t(*)>(_a[2]))); break;
        case 3: _t->handleInstpRegChanged((*reinterpret_cast< uint8_t(*)>(_a[1])),(*reinterpret_cast< uint16_t(*)>(_a[2]))); break;
        case 4: _t->handleCellRegChanged((*reinterpret_cast< uint8_t(*)>(_a[1])),(*reinterpret_cast< uint16_t(*)>(_a[2]))); break;
        case 5: _t->handleAllData((*reinterpret_cast< QDateTime(*)>(_a[1])),(*reinterpret_cast< QVector<double>(*)>(_a[2])),(*reinterpret_cast< QVector<double>(*)>(_a[3])),(*reinterpret_cast< QVector<double>(*)>(_a[4])),(*reinterpret_cast< QVector<double>(*)>(_a[5]))); break;
        case 6: _t->handleLogDirChanged((*reinterpret_cast< QDir(*)>(_a[1]))); break;
        case 7: _t->handleSaveLogFile((*reinterpret_cast< QString(*)>(_a[1]))); break;
        case 8: { bool _r = _t->openXmlFile();
            if (_a[0]) *reinterpret_cast< bool*>(_a[0]) = _r; }  break;
        case 9: _t->closeXmlFile((*reinterpret_cast< QString(*)>(_a[1]))); break;
        case 10: _t->closeXmlFile(); break;
        case 11: _t->deleteLogFile(); break;
        case 12: _t->writeXmlData(); break;
        default: ;
        }
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        switch (_id) {
        default: *reinterpret_cast<int*>(_a[0]) = -1; break;
        case 5:
            switch (*reinterpret_cast<int*>(_a[1])) {
            default: *reinterpret_cast<int*>(_a[0]) = -1; break;
            case 4:
            case 3:
            case 2:
            case 1:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< QVector<double> >(); break;
            }
            break;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        void **func = reinterpret_cast<void **>(_a[1]);
        {
            typedef void (DataLogger::*_t)(QString );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&DataLogger::newLogFileOpened)) {
                *result = 0;
                return;
            }
        }
        {
            typedef void (DataLogger::*_t)(QString );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&DataLogger::failedToOpen)) {
                *result = 1;
                return;
            }
        }
    }
}

const QMetaObject DataLogger::staticMetaObject = {
    { &QObject::staticMetaObject, qt_meta_stringdata_DataLogger.data,
      qt_meta_data_DataLogger,  qt_static_metacall, Q_NULLPTR, Q_NULLPTR}
};


const QMetaObject *DataLogger::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *DataLogger::qt_metacast(const char *_clname)
{
    if (!_clname) return Q_NULLPTR;
    if (!strcmp(_clname, qt_meta_stringdata_DataLogger.stringdata0))
        return static_cast<void*>(const_cast< DataLogger*>(this));
    return QObject::qt_metacast(_clname);
}

int DataLogger::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QObject::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 13)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 13;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 13)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 13;
    }
    return _id;
}

// SIGNAL 0
void DataLogger::newLogFileOpened(QString _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}

// SIGNAL 1
void DataLogger::failedToOpen(QString _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 1, _a);
}
QT_END_MOC_NAMESPACE
